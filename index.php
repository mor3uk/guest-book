<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Главная</title>
</head>
<body>
    <div>
        <a href="/pages/leave-message.php">Оставить сообщения</a>
    </div>
    <?php
        require "./db.php";

        // Для навигации по страницам сообщений
        $page_offset = 0;

        $query = "SELECT COUNT(*) as count FROM messages";

        if (!$res = mysqli_query($db, $query)) {
            die(mysqli_error($db));
        }

        $messages_count = mysqli_fetch_assoc($res)['count'];
        $messages_rest = $messages_count % 10;
        $page_max = (($messages_count - $messages_rest) / 10) + 1;

        // Для сортировки
        $sort_by = 'UNIX_TIMESTAMP(created_at)';

        if (isset($_GET['sortBy'])) {
            $order = 'ASC';
            if (isset($_GET['order'])) {
                switch ($_GET['order']) {
                    case 0:
                        $order = 'ASC';
                        break;
                    case 1:
                        $order = 'DESC';
                }
            }

            switch ($_GET['sortBy']) {
                case 'date':
                    $sort_by = 'UNIX_TIMESTAMP(created_at)';
                    break;
                case 'name':
                    $sort_by = 'user_name';
                    break;
                case 'email':
                    $sort_by = 'user_email';
                    break;
                default:
                    $sort_by = 'UNIX_TIMESTAMP(created_at)';
            }
        }

        if (isset($_GET['page'])) {
            $page_offset = $_GET['page'];
        }

        $query = "SELECT message, user_name, user_email, created_at FROM messages  ORDER BY " . $sort_by . " " . $order .  " LIMIT 10 OFFSET " . $page_offset * 10;

        if (!$res = mysqli_query($db, $query)) {
            die(mysqli_error($db));
        }

        if (mysqli_num_rows($res) === 0) {
            echo '<div>Список сообщений пуст!</div>';
        } else {
            echo '<table>
                <tr>
                    <th>Сообщение</th>
                    <th>Пользователь</th>
                    <th>Почтовый адрес</th>
                    <th>Дата отправки</th>
               </tr>';

            while ($row = mysqli_fetch_assoc($res)) {
                echo "<td>${row['message']}</td>
                    <td>${row['user_name']}</td>
                    <td>${row['user_email']}</td>
                    <td>${row['created_at']}</td>
                    </tr>";
            }

            echo '</table>';
        }

    ?>

    <div>
        <div>Навигация</div>
        <?php
            $query_sort_by = '';
            if (isset($_GET['sortBy'])) {
                $query_sort_by .= '&sortBy=' . $_GET['sortBy'];
                if (isset($_GET['order'])) {
                    $query_sort_by .= '&order=' . $_GET['order'];
                }
            }

            if ($page_offset > 0) {
                $prev_link = '?page=' . ($page_offset - 1) . $query_sort_by;
                echo "<a href=" . $prev_link . ">Назад</a>";
            }
            if ($page_offset >= 0 && $page_max > $page_offset + 1) {
                $next_link = '?page=' . ($page_offset + 1) . $query_sort_by;
              echo "<a href=" . $next_link . ">Вперед</a>";
          }
       ?>
    </div>
    <div>
        <span>Сортировать по: </span>

        <?php
            $query_page_offset = "";
            if (isset($_GET['page'])) {
                $query_page_offset = 'page=' . $page_offset . '&';
                if (isset($_GET['order'])) {
                    $query_page_offset .= 'order=' . $_GET['order'] . '&';
                }
            }
        ?>

        <div>
            <a href="?<?=$query_page_offset?>sortBy=name">имени пользовтеля</a>
            <a href="?<?=$query_page_offset?>sortBy=email">почтовому адресу</a>
            <a href="?<?=$query_page_offset?>sortBy=date">дате добавления</a>
        </div>

        <?php
            $current_link = '?page=' . ($page_offset) . $query_sort_by . '&';

            if (isset($_GET['sortBy'])) {

        ?>

        <span>Порядок: </span>
        <div>
            <a href="<?=$current_link?>order=1">по возрастанию</a>
            <a href="<?=$current_link?>order=0">по убывнию</a>
        </div>

        <?php
            }
        ?>
    </div>
</body>
</html>