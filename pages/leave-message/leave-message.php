<?php
    require "../db.php";

    $ip = '';
    $username = '';
    $email = '';
    $homepage = '';
    $message = '';
    $errors = [];

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $ip = $_SERVER['REMOTE_ADDR'];

        if (!empty(trim($_POST['username']))) {
            $username = secureString($_POST['username']);
            if (!preg_match('/^[a-zA-Z]+$/', $username)) {
                $errors['username'][] = 'Поле должно состоять из латинских букв';
            }
        } else {
            $errors['username'][] = 'Поле обязательно для заполнения';
        }

        if (!empty($_POST['email'])) {
            $email = secureString($_POST['email']);
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errors['email'][] = 'Email адрес введен некорректно';
            }
        } else {
            $errors['email'][] = 'Поле обязательно для заполнения';
        }

        if (!empty($_POST['homepage'])) {
            $homepage = secureString($_POST['homepage']);
            if (!filter_var($homepage, FILTER_VALIDATE_URL)) {
                $errors['homepage'][] = 'URL страницы введен некорректно';
            }
        }

        if (!empty(trim($_POST['message']))) {
            $message = secureString($_POST['message']);
        } else {
            $errors['message'][] = 'Поле обязательно для заполнения';
        }

        if (count($errors) === 0) {
            $query = "INSERT INTO messages (user_name, user_email, message, ip) VALUES ('" . $username . "', '" . $email . "', '" . $message . "', '" . $ip . "')";

            if (!mysqli_query($db, $query)) {
                echo mysqli_error($db);
            } else {
                header('Location: http://' . $_SERVER['HTTP_HOST']);
                die();
            }
        }

    }

    function secureString($value)
    {
        $value = trim($value);
        $value = htmlspecialchars($value);
        $value = stripcslashes($value);

        return $value;
    }
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Оставить сообщение</title>
</head>
<body>

<div>
    <a href="/">Перейти к сообщениям</a>
</div>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
    <div>
        <input name="username" type="text" value="<?=@$username?>" placeholder="Имя пользователи">
        <div><?=@$errors['username'][0]?></div>
    </div>
    <div>
        <input name="email" type="text" value="<?=@$email?>" placeholder="Email">
        <div><?=@$errors['email'][0]?></div>
    </div>
    <div>
        <input name="homepage" type="text" value="<?=@$homepage?>" placeholder="Страница пользователя">
        <div><?=@$errors['homepage'][0]?></div>
    </div>
    <div>
        <input name="message" type="text" value="<?=@$message?>" placeholder="Текст сообщения">
        <div><?=@$errors['message'][0]?></div>
    </div>
    <button>Оставить сообщение</button>
</form>

</body>
</html>
