<?php

require "config.php";

$db = mysqli_connect($host, $username, $password, $dbname);

if (mysqli_connect_errno()) {
    echo mysqli_connect_error();
}

$query = "CREATE TABLE IF NOT EXISTS messages (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_name VARCHAR(32) NOT NULL,
    user_email VARCHAR(32) NOT NULL,
    created_at DATETIME DEFAULT now(),
    message TEXT NOT NULL,
    ip VARCHAR(45) NOT NULL 
)";

if (!mysqli_query($db, $query)) {
    echo mysqli_error($db);
}